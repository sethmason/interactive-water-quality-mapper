


var realtime_store;
var loc = window.location.pathname;
var dir = loc.substring(0, loc.lastIndexOf('/'));
var Mygrid;
var stationGrid=[];
var map;
var bounding_flag = false;
var northwest=null;
var southeast=null;
var characteristicNames=[];
var station_info="";
var station_json="";
var station_store; //has the initial stations
var markerJson = [];
var facility_store;

var markerLayer = [];

var slickGrid;
var columns = [
  {id: "term", name: "Characteritic Name", field: "term",width: 300, sortable: true},
  {id: "unit", name: "Unit", field: "unit",width: 80, sortable: true},
  {id: "medium", name: "Medium", field: "medium",width: 120, sortable: true},
  {id: "code", name: "USGS-code", field: "code",width: 170, sortable: true},
  {id: "start", name: "Collection Start Date", field: "start",width: 170, sortable: true},
  {id: "end", name: "Collection End Date", field: "end",width: 170, sortable: true}
];

var options = {
  enableCellNavigation: true,
  enableColumnReorder: true,
  multiColumnSort: true
};


//close the preloader div
function endLoading() {
  require(["dojo/_base/declare","dojo/dom","dojo/dom-style", "dojo/_base/fx"], function(declare, dom, domStyle, fx){
    // called to hide the loading overlay
        // fade the overlay gracefully
        fx.fadeOut({
            node: dom.byId("preloader"),
            onEnd: function(node){
                domStyle.set(node, 'display', 'none');
            }
        }).play();
    });
};


//This adds the information slide panels to each tab
//and adds some functionality to the query dialog 
//Author: Seth Mason and Sean Cleveland
//Date Modified: 12-12-12
$(document).ready(function(){
    $('#mapPanel').slidePanel({
        triggerName: '#trigger1',
        position: 'fixed',
        triggerTopPos: '360px',
        panelTopPos: '200px',
        ajax: true,
        ajaxSource: './data/subpages/mapPanel.html'
    });
    $('#sitesPanel').slidePanel({
        triggerName: '#trigger2',
        position: 'fixed',
        triggerTopPos: '360px',
        panelTopPos: '200px',
        ajax: true,
        ajaxSource: './data/subpages/sitesPanel.html'
    });
    $('#facilitiesPanel').slidePanel({
        triggerName: '#trigger3',
        position: 'fixed',
        triggerTopPos: '360px',
        panelTopPos: '200px',
        ajax: true,
        ajaxSource: './data/subpages/facilitiesPanel.html'
    });
    $('#cdxPanel').slidePanel({
        triggerName: '#trigger4',
        position: 'fixed',
        triggerTopPos: '360px',
        panelTopPos: '200px',
        ajax: true,
        ajaxSource: './data/subpages/cdxPanel.html'
    });
    $('input[name=variablechoice]').click(function() {
       if (this.checked && $(this).val() == 'characteristics'){
         $('#characteristic_div input').each(function() {
             $(this).attr('disabled',false);
         });
         $('#characteristic_div').css('background-color','#ccffcc'); 
         $('#characteristic_type_div input').each(function() {
              $(this).attr('disabled',true);
         });
         $('#characteristic_type_div').css('background-color','#C8C8C8');
         $('#var_choice').val('characteristics');
       }
       else {
         $('#characteristic_div input').each(function() {
              $(this).attr('disabled',true);
          });
          $('#characteristic_div').css('background-color','#C8C8C8'); 
          $('#characteristic_type_div input').each(function() {
                $(this).attr('disabled',false);
          });
          $('#characteristic_type_div').css('background-color','#ccffcc'); 
         $('#var_choice').val('characteristics_type');
       }
    });
});

//  initialize the app by bringing in the data layers, rendering the map and grids
function initialize(){
  require(["dojox/data/CsvStore", "dojo/store/DataStore","dojo/store/Memory"], function(CsvStore, DataStore, Memory){
    //get the data sets
    $.get("http://www.waterqualitydata.us/Station/search?huc=14010003&mimeType=csv", function(data){
      station_store = new dojox.data.CsvStore({
        identifier: "MonitoringLocationIdentifier",
        data: data
      });
      //$.getJSON("http://iaspub.epa.gov/enviro/efservice/t_compliance_echo/county/EAGLE/NPDES_ID_FLAG/Y/LATITUDE/!=/null/LONGITUDE/!=/null/JSON", function(data){
      $.getJSON("data/data/facilities.json", function(data){
        facility_store = new Memory({
            data: data,
            idProperty: 'REGISTRY_ID',
        });
        makeFacilityGrid();
        setupMap();
        makeStationGrid();
        endLoading();
      });  
    });
  });
}

//  setupGrid
//  This does the intial ajax call to the data-portal and pulls a csv of stations
//  the stations are parsed into a dojo CSV Store and displayed in a data grid 
//  lastly the call to the addStationMarkers function happens
//
//  author:  Sean Cleveland
//  last_updated: 2012-8-14
//function setupGrid() {
//    if (window.Event) {
//      document.captureEvents(Event.MOUSEMOVE);
//    }
//    
//    $.get("http://www.waterqualitydata.us/Station/search?huc=14010003&mimeType=csv", function(data){
//      station_store = new dojox.data.CsvStore({
//          identifier: "MonitoringLocationIdentifier",
//          data: data
//      });
//
//    var set_grid_formatter = {
//      info: function(value) {
//        some_str="help";
//        return '<a href="#" onClick="addStationTab(\''+value.toString()+'\');"><img src="data/images/find-small.png"></a>';
//      }
//    };
//    // set the layout structure:
//    var layout = [
//    {
//        field: 'MonitoringLocationIdentifier',
//        name: 'Info',
//        formatter: set_grid_formatter.info,
//        width: '30px'},
//    {
//        field: 'MonitoringLocationIdentifier',
//        name: 'Station ID',
//        width: '220px'},
//    {
//        field: 'MonitoringLocationName',
//        name: 'Name',
//        width: '400px'},
//    {
//        field: 'MonitoringLocationTypeName',
//        name: 'Station Type',
//        width: '120px'},
//    {
//        field: 'LatitudeMeasure',
//        name: 'Latitude',
//        width: '120px'},
//    {
//        field: 'LongitudeMeasure',
//        name: 'Longitude',
//        width: '120px'}
//    ];
//    
//    // create a new grid:
//    Mygrid = new dojox.grid.EnhancedGrid({
//        store: station_store,
//        id: "datagrid",
//        keepSelection: true,
//        clientSort: true,
//        selectionMode: "extended",
//        columnReordering: true,
//        loadingMessage: "Loading data...",
//        structure: layout,
//        //rowSelector: '20px',
//        plugins:{
//          filter:true,indirectSelection:{headerSelector:true, width:"40px", styles:"text-align: center;"}
//        }
//    }, document.createElement('div'));
//    dojo.byId("303d_list").appendChild(Mygrid.domNode);
//    Mygrid.startup();
//  });
//};

//makeStationGrid
//Parse station data and
//setup monitoring station Grid
//
//author: Seth Mason
//last updated: 1-3-13
function makeStationGrid(){
  require([
	"gridx/Grid",
	"gridx/core/model/cache/Async",
	"gridx/modules/VirtualVScroller",
	"gridx/modules/ColumnResizer",
        "gridx/modules/Focus",
	"gridx/modules/extendedSelect/Row",
        "gridx/modules/Select/Row",
	"gridx/modules/SingleSort",
	"gridx/modules/filter/Filter",
	"gridx/modules/RowHeader",
	"gridx/modules/IndirectSelect",
	"gridx/modules/filter/FilterBar",
	"dojo/store/Memory",
        "dojo/dom",
        "dojo/ready",
	"dojo/domReady!"
    ], function(Grid, Cache, 
	VirtualVScroller, ColumnResizer, SelectRow, 
	SingleSort, Filter, RowHeader, Focus, ExtendedSelectRow,
	IndirectSelect, FilterBar, dom, ready){
        var cacheClass = "gridx/core/model/cache/Async";
        var stationGrid = new Grid({
                id: 'stationGrid',
                cacheClass: Cache,
		store: station_store, 
		structure: [  
                {
                    id: 'MonitoringLocationIdentifier',
                    field:'MonitoringLocationIdentifier', 
                    name: 'Station ID',
                    width: 'auto'},
                
                {
                    id: 'MonitoringLocationName',
                    field: 'MonitoringLocationName',
                    name: 'Name',
                    width: 'auto'},
                {
                    id:'MonitoringLocationTypeName',
                    field: 'MonitoringLocationTypeName',
                    name: 'Station Type',
                    width: 'auto'},
                {
                    id: 'LatitudeMeasure',
                    field: 'LatitudeMeasure',
                    name: 'Latitude',
                    width: 'auto'},
                {
                    id: 'LongitudeMeasure',
                    field: 'LongitudeMeasure',
                    name: 'Longitude',
                    width: 'auto'}
                ],
		selectRowTreeMode: false,
		modules: [
                    VirtualVScroller,
                    Focus,
                    ColumnResizer,
                    SingleSort,
                    Filter,
                    FilterBar,
                    RowHeader,
                    IndirectSelect,
                    ExtendedSelectRow,
                    SelectRow,
		]
	});
        stationGrid.connect(stationGrid, 'onCellDblClick', function(evt){
            addStationTab(evt.rowId);
        });
        stationGrid.placeAt("wq_stations_tab");
	stationGrid.startup();
    });
}

function showStationVariables(station_id){
  dijit.byId("loadingDialog").show()
  station_store.fetch({query: { MonitoringLocationIdentifier: station_id}, onComplete: displayVariables}); 
}

function displayVariables(items, request){  $.getJSON('http://skmason-envi.com/iwqm-cake/stations_variables/by_name/'+station_store.getValue(items[0],"MonitoringLocationIdentifier")+'.json', function(results) {
      station_json = results;
      data = [];
      var variable_content = "<h2>"+station_store.getValue(items[0],"MonitoringLocationIdentifier")+" Variables:</h2><table id='"+station_store.getValue(items[0],"MonitoringLocationIdentifier")+"_table' class='variable_data' border='1' ><thead><tr><th>Characteristic Name</th><th>Unit</th><th>Medium</th><th>Variable Code</th><th>Collection Start Date</th><th>Collection End Date</th></tr></thead><tbody>";
      for(x in station_json.data.Variable){
        data[x] = {
                term: station_json.data.Variable[x].term,
                unit: station_json.data.Variable[x].unit,
                medium: station_json.data.Variable[x].medium,
                code: station_json.data.Variable[x].code,
                start: station_json.data.Variable[x].start,
                end: station_json.data.Variable[x].end
              };
        variable_content = variable_content + '<tr><td>'+station_json.data.Variable[x].term+'</td><td>'+station_json.data.Variable[x].unit+'</td><td>'+station_json.data.Variable[x].medium+'</td><td>'+station_json.data.Variable[x].code+'</td><td>'+station_json.data.Variable[x].start+'</td><td>'+station_json.data.Variable[x].end+'</td></tr>';
      }
      variable_content = variable_content + '</tbody></table>'
      $('#var_data_div').html("<h3>Station ID:"+station_store.getValue(items[0],"MonitoringLocationIdentifier")+"</h3><br><h3>Variables:</h3>");
      data.sort(SortByTerm)
      slickGrid = new Slick.Grid("#varGrid", data, columns, options);
      slickGrid.onSort.subscribe(function (e, args) {
            var cols = args.sortCols;

            data.sort(function (dataRow1, dataRow2) {
              for (var i = 0, l = cols.length; i < l; i++) {
                var field = cols[i].sortCol.field;
                var sign = cols[i].sortAsc ? 1 : -1;
                var value1 = dataRow1[field], value2 = dataRow2[field];
                var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
                if (result != 0) {
                  return result;
                }
              }
              return 0;
            });
            slickGrid.invalidate();
            slickGrid.render();
          });
      dijit.byId("loadingDialog").hide()
    });
}


function addStationTab(station_id){
  require(["dijit/registry"], function(registry){
    dijit.byId("loadingDialog").show()
    if (dijit.byId("station_"+station_id)) {
      var mainTabs = registry.byId("main-tab-container");
      var siteTabs = registry.byId("sites-tab-container");
      siteTabs.selectChild(dijit.byId(tab_id));
      mainTabs.selectChild(dijit.byId("site_tab"));
      dijit.byId("loadingDialog").hide()
    }
    else {
      station_store.fetch({query: { MonitoringLocationIdentifier: station_id}, onComplete: getStationInfo});
    }
  });
}

function getStationInfo(items, request){
  require(["dijit/registry", "dijit/layout/ContentPane"], function(registry, ContentPane){
    var mainTabs = registry.byId("main-tab-container");
    var siteTabs = registry.byId("sites-tab-container");
    tab_id = "station_"+station_store.getValue(items[0],"MonitoringLocationIdentifier");
    $.getJSON('http://skmason-envi.com/iwqm-cake/stations_variables/by_name/'+station_store.getValue(items[0],"MonitoringLocationIdentifier")+'.json', function(results) {
      station_json = results;
      window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier"+"data")] =[];
      

      for(x in station_json.data.Variable){
        window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier"+"data")][x] = {
                term: station_json.data.Variable[x].term,
                unit: station_json.data.Variable[x].unit,
                medium: station_json.data.Variable[x].medium,
                code: station_json.data.Variable[x].code,
                start: station_json.data.Variable[x].start,
                end: station_json.data.Variable[x].end
              };
      }

      var pane = new ContentPane({
                  id: tab_id,
                  title: station_store.getValue(items[0],"MonitoringLocationIdentifier"),
                  closable: true,
                  content: "<h3>Station ID: "+station_store.getValue(items[0],"MonitoringLocationIdentifier")+"</h3><h3>Station Name: "+station_store.getValue(items[0],"MonitoringLocationName")+"</h3><br /><table class=tab_dataTable><tr><td>Data Collection Entity:</td><td>"+station_store.getValue(items[0],"OrganizationFormalName")+"</td></tr><tr><td>Station Type:</td><td>"+station_store.getValue(items[0],"MonitoringLocationTypeName")+"</td></tr><tr><td>Latitude:</td><td>"+station_store.getValue(items[0],"LatitudeMeasure")+"</td></tr><tr><td>Longitude:</td><td>"+station_store.getValue(items[0],"LongitudeMeasure")+"</td></tr><tr><td>HUC Code:</td><td>"+station_store.getValue(items[0],"HUCEightDigitCode")+"</td></tr><tr><td>Drainage Area:</td><td>"+station_store.getValue(items[0],"DrainageAreaMeasure/MeasureValue")+" "+station_store.getValue(items[0],"DrainageAreaMeasure/MeasureUnitCode")+"</td></tr><tr><td>Elevation:</td><td>"+station_store.getValue(items[0],"VerticalMeasure/MeasureValue")+" "+station_store.getValue(items[0],"VerticalMeasure/MeasureUnitCode")+"</td></tr><tr><td>Well Depth:</td><td>"+station_store.getValue(items[0],"WellDepthMeasure/MeasureValue")+" "+station_store.getValue(items[0],"WellDepthMeasure/MeasureUnitCode")+"</td></tr><tr><td>Geological Formation:</td><td>"+station_store.getValue(items[0],"FormationTypeText")+"</td></tr><tr><td>Aquifer Type:</td><td>"+station_store.getValue(items[0],"AquiferTypeName")+"</td></tr></table><br /><br /><h3>Station Data Summary Table:<div id='" + station_store.getValue(items[0],"MonitoringLocationIdentifier") +"' style='width:auto;height:400px;'><div>" //variable_content
              });
      
      siteTabs.addChild(pane);
      siteTabs.selectChild(pane);
      mainTabs.selectChild(dijit.byId("site_tab"));
      window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier"+"data")].sort(SortByTerm);
      window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier")] = new Slick.Grid("#"+station_store.getValue(items[0],"MonitoringLocationIdentifier"), window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier"+"data")], columns, options);
       window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier")].onSort.subscribe(function (e, args) {
            var cols = args.sortCols;

            window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier"+"data")].sort(function (dataRow1, dataRow2) {
              for (var i = 0, l = cols.length; i < l; i++) {
                var field = cols[i].sortCol.field;
                var sign = cols[i].sortAsc ? 1 : -1;
                var value1 = dataRow1[field], value2 = dataRow2[field];
                var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
                if (result != 0) {
                  return result;
                }
              }
              return 0;
            });
             window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier")].invalidate();
             window["v" + station_store.getValue(items[0],"MonitoringLocationIdentifier")].render();
          });
      dijit.byId("loadingDialog").hide()
    });
  });
}



//setup permitted discharge facility Grid
//
function makeFacilityGrid(){
  require([
	"gridx/Grid",
	"gridx/core/model/cache/Async",
	"gridx/modules/VirtualVScroller",
	"gridx/modules/ColumnResizer",
        "gridx/modules/Focus",
	"gridx/modules/extendedSelect/Row",
        "gridx/modules/Select/Row",
	"gridx/modules/SingleSort",
	"gridx/modules/filter/Filter",
	"gridx/modules/RowHeader",
	"gridx/modules/IndirectSelect",
	"gridx/modules/filter/FilterBar",
	"dojo/store/Memory",
        "dojo/dom",
        "dojo/ready",
	"dojo/domReady!"
    ], function(Grid, Cache, 
	VirtualVScroller, ColumnResizer, SelectRow, 
	SingleSort, Filter, RowHeader, Focus, ExtendedSelectRow,
	IndirectSelect, FilterBar, dom, ready){
        var cacheClass = "gridx/core/model/cache/Async";
        var facilityGrid = new Grid({
                id: 'facilityGrid',
                cacheClass: Cache,
		store: facility_store, 
		structure: [  
                {
                    id: 'REGISTRY_ID',
                    field:'REGISTRY_ID', 
                    name: 'Facility (FRS) ID',
                    width: 'auto'},
                {
                    id: 'CWA_ID',
                    field:'CWA_ID', 
                    name: 'Clean Water Act ID',
                    width: 'auto'},
                {
                    id: 'NAME',
                    field: 'NAME',
                    name: 'Facility Name',
                    width: 'auto'},
                {
                    id:'CURR_COMP_STATUS',
                    field: 'CURR_COMP_STATUS',
                    name: 'Status',
                    width: 'auto'},
                {
                    id:'LATITUDE',
                    field: 'LATITUDE',
                    name: 'Latitude',
                    width: 'auto'},
                {
                    id:'LONGITUDE',
                    field: 'LONGITUDE',
                    name: 'Longitude',
                    width: 'auto'},
                ],
		selectRowTreeMode: false,
		modules: [
                    VirtualVScroller,
                    Focus,
                    ColumnResizer,
                    SingleSort,
                    Filter,
                    FilterBar,
                    RowHeader,
                    //IndirectSelect,
                    //ExtendedSelectRow,
                    SelectRow,
		]
	});
        facilityGrid.connect(facilityGrid, 'onCellDblClick', function(evt){
            //alert(evt.rowId);
            openFacilityTab(evt.rowId)
        });
        facilityGrid.placeAt("facilities_grid");
	facilityGrid.startup();
    });
}

function openFacilityTab(id){
  require(["dijit/registry", "dijit/layout/ContentPane"], function(registry, ContentPane){
    //dijit.byId("loadingDialog").show();
    tab_id = id
    var mainTabs = registry.byId("main-tab-container");
    var facilityTabs = registry.byId("discharge-facilities-container");
    if (dijit.byId("discharge-facilities-container_tablist_"+tab_id)){
        facilityTabs.selectChild(tab_id);
        mainTabs.selectChild(dijit.byId("facilities_tab"));
        //dijit.byId("loadingDialog").hide()
    }
    else{
        var data = facility_store.get(id)
        var pane = new ContentPane({
            id: tab_id,
            title: tab_id,
            closable: true,
            content: "<h3>EPA Facility Registry System (FRS) ID: " + data.REGISTRY_ID +"</h3><h3>Facility Name: " + data.NAME + "</h3><br /><table class=tab_dataTable><tr><td>Clean Water Act ID(s)</td><td>" + data.CWA_ID + "</td></tr><tr><td>Discharge Permit Type</td><td>" + data.PERMIT_TYPE + "</td></tr><tr><td>Address</td><td>" + data.ADDRESS + "</td></tr><tr><td>City</td><td>" + data.CITY + "</td></tr><tr><td>State</td><td>" + data.STATE + "</td></tr><tr><td>ZIP</td><td>" + data.ZIP + "</td></tr><tr><td>Current Permit Compliance Status</td><td>" + data.CURR_COMP_STATUS_CWA + "</td></tr><tr><td>Days Since Last Inspection</td><td>" + data.DAY_SINCE_LAST_INSP_CWA + "</td></tr><tr><td>Quarters in Noncompliance in Previous 3 Years </td><td>" + data.QTR_NON_COMP_3_YRS_CWA + "</td></tr><tr><td>Count of Informal Enforcement Actions in Previous 5 Years </td><td>" + data.INF_ENF_ACT_NOVS_5YRS_CWA + "</td></tr><tr><td>Count of Formal Enforcement Actions in Previous 5 Years </td><td>" + data.FORMAL_ENF_ACT_5YR_CWA + "</td></tr><tr><td>Date of Last Formal Enforcement Action </td><td>" + data.DATE_LAST_FORMAL_ACT_CWA + "</td></tr><tr><td>Total Penalties in Previous 5 Years </td><td>$" + data.PENALTIES_5_YRS_CWA + "</td></tr><tr><td>Date of Last Penalty </td><td>" + data.DATE_OF_LAST_PENALTY_CWA + "</td></tr><tr><td>Amount of Last Penalty </td><td>$" + data.AMOUNT_OF_LAST_PENALTY_CWA + "</td></tr><tr><td>Link to Compliance Report</td><td><a target='_blank' href=	http://www.epa-echo.gov/cgi-bin/get1cReport.cgi?tool=echo&IDNumber=" + data.REGISTRY_ID+ ">" + data.COMPLIANCE_REPORT + "</a></td></tr></table>"
        });
        facilityTabs.addChild(pane);
        facilityTabs.selectChild(pane);
        mainTabs.selectChild(dijit.byId("facilities_tab"));
        //dijit.byId("loadingDialog").hide()        
    };
  });
}


//  setupMap
//
//  author:  Sean Cleveland and Seth Mason
//  last_updated: 2012-11-21
function setupMap() {
  require(["dojox/data/CsvStore", "dojo/store/DataStore"], function(CsvStore, DataStore) {
  var layers = document.getElementById('map-ui');
  
  var southwest = new L.LatLng(38.8, -108.1);
  var northeast = new L.LatLng(40.4, -104.8);
  bounding_area = new L.LatLngBounds(southwest, northeast);
  
  //initialize map using Leaflet api  
  var map = L.map('map', {
    center: [39.6, -106.5],
    zoom:10,
    minZoom: 9,
    zoomControl: false,
    //maxBounds: bounding_area,
  });
  //set default baselayer to Stamen Terrain
  var baselayer = new L.StamenTileLayer("terrain");
  map.addLayer(baselayer);
  map.invalidateSize({
    animate: true,
  });
  //add attribution
  map.attributionControl.setPrefix('Powered by Leaflet; <a href="http://skmason-envi.com" target="_blank">&copy; 2012, S.K.Mason Environmental, LLC </a>');
  
  //add a scale bar
  L.control.scale().addTo(map);

  //add watershed boundary layer
  var watershedStyle = {
    "color": "yellow",
    "weight": 4,
    "opacity": 0.3
  };
  var watershedTemp = []
  var watershed = new L.GeoJSON(watershedTemp, {
    style: watershedStyle,
  });
  $.getJSON("data/data/watershed.geojson", function(data){
    watershed.addData(data);  
  });
  map.addLayer(watershed);
  
  var wqStation_marker = L.icon({
    iconUrl: "data/images/wq_station_marker.png",
    iconSize: [22,24],
    iconAnchor: [11,24],
    popupAnchor: [0, -20],
    shadowUrl: "data/images/round_marker_shadow.png",
    shadowSize: [25,15],
    shadowAnchor:[5,12],
  });
  
  var markerLayer = new L.LayerGroup();
  map.addLayer(markerLayer);
  
   var gotList = function(items, request){
     var itemsList = "";
     var lat = "";
     var lng="";

     dojo.forEach(items, function(i){
       lat = station_store.getValue(i, "LatitudeMeasure");
       lng = station_store.getValue(i, "LongitudeMeasure");
       id = station_store.getValue(i, "MonitoringLocationIdentifier");
       site_name = station_store.getValue(i, "MonitoringLocationName");
       site_type = station_store.getValue(i, "MonitoringLocationTypeName");
       
        var marker = new L.Marker(new L.latLng([lat, lng]), {title: site_name});
        marker.bindPopup('<span><b>WATER QUALITY MONITORING STATION</b></span><br><span><b>Station ID:</b> ' +id+ '</span><br><span><b>Station Name:</b> ' +site_name + '</span><br><span><b>Station Type:</b> ' +site_type + '</span><br><button class="button" id="btn_'+id+'" onclick="addStationTab(\''+id+'\');">Show More Information</button>');
        markerLayer.addLayer(marker);
     });
     markerLayer.eachLayer(function (marker) {
        marker.setIcon(wqStation_marker);
    });
   }
   
   var gotError = function(error, request){
     alert("The request to the store failed. " +  error);
   }
   // Invoke the search
   station_store.fetch({
     onComplete: gotList,
     onError: gotError
   });
  
  //add 303(d) listed reaches
  var impairedStyle = {
    "color": "red",
    "weight": 5,
    "opacity": 0.65
  };
  var temp303d = []
  var impaired303d = new L.GeoJSON(temp303d, {
    style: impairedStyle,
    onEachFeature: function (feature, layer) {
    layer.bindPopup('<span><b>303(d) LISTED STREAM SEGMENT</b></span><br><span><b>Segment ID:</b> ' +feature.properties.SOURCE_FEA+ '</span><br><span><b>Segment Length:</b> ' +feature.properties.SHAPE_LENG + ' mi.</span><br><button class="button" onclick="window.open(\''+feature.properties.FEATUREDET+'\');">Show More Information</button>');
    }
  });
  $.getJSON("data/data/303d.geojson", function(data){
    impaired303d.addData(data); 
    });
  map.addLayer(impaired303d);
  
  //add active TMDLs
  var TMDLStyle = {
    "color": "green",
    "weight": 5,
    "opacity": 0.65
  };
  TMDLtemp = []
  var TMDL = new L.GeoJSON(TMDLtemp, {
    style: TMDLStyle,
    onEachFeature: function (feature, layer) {
    layer.bindPopup('<span><b>ACTIVE TMDL</b></span><br><span><b>TMDL Code:</b> ' +feature.properties.SOURCE_FEA+ '</span><br><span><b>Segment ID:</b> ' +feature.properties.SOURCE_DAT + '</span><br><button class="button" onclick="window.open(\''+feature.properties.FEATUREDET+'\');">Show More Information</button>');
    }
  });
  $.getJSON("data/data/TMDL.geojson", function(data){
    TMDL.addData(data); 
    });
  map.addLayer(TMDL);  

  ////add nonpoint source projects
  //var nonpointStyle = {
  //  "color": " 	#00FFFF",
  //  "weight": 5,
  //  "opacity": 0.65
  //};
  //nonpointTemp = []
  //var nonpointProjects = new L.GeoJSON(nonpointTemp, {
  //  style: nonpointStyle,
  //  onEachFeature: function (feature, layer) {
  //  layer.bindPopup('<span><b>Nonpoint Source Project</b></span><br><span><b>Project ID:</b> ' +feature.properties.COMID+ '</span><br><span><b>Reach ID:</b> ' +feature.properties.REACHCODE + '</span><br><button class="button" onclick="window.open(\''+feature.properties.FEATUREDET+'\');">Show More Information</button>');
  //  }
  //});
  //$.getJSON("nonpointSourceProjects.geojson", function(data){
  //  nonpointProjects.addData(data); 
  //  });
  //map.addLayer(nonpointProjects);

 //add permitted dischargers
  var facility_marker = L.icon({
    iconUrl: "data/images/outfall_marker.png",
    iconSize: [27,30],
    iconAnchor: [13,30],
    popupAnchor: [0, -25],
    shadowUrl: "data/images/square_marker_shadow.png",
    shadowSize: [45,19],
    shadowAnchor:[5,15],
  });
  
  var facilityLayer = new L.LayerGroup();
  map.addLayer(facilityLayer);
  
  facility_store.query({NPDES_ID_FLAG: "Y"}).forEach(function(i){
       lat = i.LATITUDE;
       lng = i.LONGITUDE;
       frs_id = i.REGISTRY_ID;
       facility_name = i.NAME;
       facility_status = i.CURR_COMP_STATUS;
       
        var marker = new L.Marker(new L.latLng([lat, lng]), {title: facility_name});
        marker.bindPopup('<span><b>PERMITTED DISCHARGE FACILITY</b></span><br><span><b>EPA Facility (FRS) ID:</b> ' +frs_id+ '</span><br><span><b>Facility Name:</b> ' +facility_name + '</span><br><span><b>Compliance Status:</b> ' + facility_status + '</span><br><button class="button" id="btn_'+id+'" onclick="openFacilityTab(\''+frs_id+'\');">Show More Information</button>');
        facilityLayer.addLayer(marker);
     });

  facilityLayer.eachLayer(function (marker) {
      marker.setIcon(facility_marker);
  });
 

  //add realtime monitoring stations
  var realtime_marker = L.icon({
    iconUrl: "data/images/streamgauge_marker.png",
    iconSize: [27,30],
    iconAnchor: [13,30],
    popupAnchor: [0, -25],
    shadowUrl: "data/images/square_marker_shadow.png",
    shadowSize: [45,19],
    shadowAnchor:[5,15],
  });
  
  var tempRealtime = []
  var realtimeStations = new L.GeoJSON(tempRealtime, {
    pointToLayer: function (feature, latlng) {
	return L.marker(latlng, {icon: realtime_marker});
    },
    onEachFeature: function (feature, layer) {
    layer.bindPopup('<span><b>Realtime Monitoring Station</b></span><br><span><b>Station ID:</b> ' +feature.properties.Name+ '</span><br><span><b>Station Name:</b> ' +feature.properties.Description + '</span><br><button class="button" onclick="window.open(\'http://waterdata.usgs.gov/nwis/inventory?agency_code=USGS&site_no='+feature.properties.Name+'\');">Show More Information</button>');
    }
  });
  $.getJSON("data/data/realtimeSites.geojson", function(data){
    realtimeStations.addData(data); 
    });
  map.addLayer(realtimeStations);
  
  
  RefreshControl = function(refreshButton) {

    var control = new (L.Control.extend({
    options: { position: 'topright' },
    onAdd: function (map) {
        controlDiv = L.DomUtil.create('div', 'refreshMapControl');
        L.DomEvent
            .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
            .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
            .addListener(controlDiv, 'click', this.refreshMap);
        controlDiv.title = 'Refresh Map';
        return controlDiv;
    }
    }));
    control.refreshMap = refreshButton;
    return control;
    };
    
    refreshMap = function () {
        map.invalidateSize();
        map.panTo(new L.LatLng(39.6, -106.5));
        map.setZoom(10);
    };

    map.addControl(RefreshControl(refreshMap));
 
  
  //add layer switcher
  var baseMaps = {
    "Terrain": baselayer,
  };
  var overlayMaps = {
    "Watershed": watershed,
    "Monitoring Stations": markerLayer,
    "303(d) Listed Reaches": impaired303d,
    "Permitted Dischargers": facilityLayer,
    "Active TMDLs": TMDL,
    "Realtime Monitoring": realtimeStations,
    //"Nonpoint Source Projects": nonpointProjects,
  };
  L.control.layers(baseMaps, overlayMaps).addTo(map);

  //add location filter
  var locationFilter = new L.LocationFilter().addTo(map);  
  var filterButtons = new L.Control.ButtonContainer().addTo(map);
  
  locationFilter.on("enabled", function () {
    // Do something when enabled.   
    var bounds = locationFilter.getBounds();
    var bbox = bounds.toBBoxString();
    document.getElementById('bbox_field').value = bbox;
    $("#button_container").append('<a class="query-button" href="#" id="query_button" onClick ="openMapQueryDialog()">Retrieve Water Quality Data</a>'); //add a query button   
  });
  
  locationFilter.on("change", function (e) {
    // Do something when the bounds change.
    // Bounds are available in `e.bounds`.
    var bounds = locationFilter.getBounds();
    var bbox = bounds.toBBoxString();
    document.getElementById('bbox_field').value = bbox;
  });
  
  locationFilter.on("disabled", function () {
    // Do something when disabled.
    $("a#query_button").remove(); //remove the query button
    map.panTo(new L.LatLng(39.6, -106.5));
    map.setZoom(10);
  });

  //add zoom control beneath location filter
  L.control.zoom().addTo(map);
  
  //add search control
  map.addControl( new L.Control.Search({searchLayer: markerLayer}) );
  
  });
}


//  openMapQueryDialog
//  Shows the map query dialog and finds the stations contained in the bounding box
//  then populates the characteristics from those stations.
//
//  author:  Sean Cleveland
//  last_updated: 2012-11-29
function openMapQueryDialog(station_flag){
  dijit.byId("loadingDialog").show()
  if(station_flag == null){document.getElementById('bbox_field')
    $('#bbox_label').empty().html("Bounding Box:");
    bbox = document.getElementById('bbox_field').value;
    wqd_url = "http://www.waterqualitydata.us/Station/search?huc=14010003&bBox="+bbox+"&mimeType=xml";
    $.get(wqd_url, function(data){
      url_string = "http://skmason-envi.com/iwqm-cake/stations_variables/characteristics_by_station_name.json?"
      stations = [];
      $(data).find("WQX").find("Organization").each(function(){
        $(this).find("MonitoringLocation").each(function()
        {
              //url_string = url_string + "&stations[]=" + $(this).find("MonitoringLocationIdentifier").text();
              stations.push($(this).find("MonitoringLocationIdentifier").text());
        });
      });
      $.post(url_string,{'stations[]': stations},function(chars){
       // c = dijit.byId("characteristicName");
        $('#characteristic_div').empty();
        $.each(chars["characteristics"], function(i, item) {
        //  c.addOption({value: item, label: item})
          $('#characteristic_div').append('<label><input type="checkbox" name="characteristic_group[]" value="'+item+'" />  '+item+'</label><br />');
        });
        dijit.byId("loadingDialog").hide();
        mapDialog.show();
      },'json')
    }, 'xml');
  }
  else{
    $('#bbox_label').empty().html("Stations:");
    //stations = [];
    var grid_items = dijit.byId("stationGrid").select.row.getSelected();
    //alert(grid_items);
    //for (var i=0;i<grid_items.length;i++)
    //{ 
    //  stations.push(grid_items[i]._csvId);
    //}
    $('#bbox_field').val(grid_items.join(','));
    url_string = "http://skmason-envi.com/iwqm-cake/stations_variables/characteristics_by_station_name.json?"
    $.post(url_string,{'stations[]': grid_items},function(chars){
    //$.post(url_string,{'stations[]': stations},function(chars){
     // c = dijit.byId("characteristicName");
      $('#characteristic_div').empty();
      $.each(chars["characteristics"], function(i, item) {
      //  c.addOption({value: item, label: item})
        $('#characteristic_div').append('<label><input type="checkbox" name="characteristic_group[]" value="'+item+'" class="characteristic"/>  '+item+'</label><br />');
      });
      dijit.byId("loadingDialog").hide();
      mapDialog.show();
    },'json')
  }
  
  
}

//  doSearch
//  This constructs the waterqualitydata.us query string for the bounding box search 
//  and then submits the query for download.
//
//  author:  Sean Cleveland
//  last_updated: 2012-11-30
function doSearch(){
  dijit.byId("loadingDialog").show();
  bbox = document.getElementById('bbox_field').value;
  start_date = $('#startDateLo').val();
  end_date = $('#startDateHi').val();
  //Get the Characteristics
  characteristics = new Array();
  $.each($("input[name='characteristic_group[]']:checked"), function() {
    characteristics.push($(this).val());
  });
  characteristic_types = new Array();
  $.each($("input[name='characteristic_type_group[]']:checked"), function() {
    characteristic_types.push($(this).val());
  });
   urlstring =  'http://www.waterqualitydata.us/Result/search?&huc=14010003';
   if($('#var_choice').val() == 'characteristics'){
       urlstring = urlstring + '&characteristicName=' + characteristics.join(';');
    }
    else{
      urlstring = urlstring + '&characteristicType=' + characteristic_types.join(';');
    }
   if($('#bbox_label').html().indexOf("Stations") !== -1){
     urlstring = urlstring + "&siteid="+bbox.replace(',',';');
   }
   else{
     urlstring = urlstring + "&bBox="+bbox;
   }
   urlstring = urlstring + "&startDateLo=" + start_date;
   urlstring = urlstring + "&startDateHi=" + end_date;
   urlstring=  urlstring + '&mimeType='+ $('#file_type').val() + '&zip=yes';
   //alert(urlstring);
   downloadURL(urlstring);
}

//  downloadURL
//  create a hidden iframe for downloading a submited url that returns a file
//
//  author:  Sean Cleveland
//  last_updated: 2012-11-30
function downloadURL(url) {
    var iframe;
    var hiddenIFrameID = 'hiddenDownloader';
    iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');  
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;   
    dijit.byId("loadingDialog").hide();
    dijit.byId("mapDialog").hide();
}

function mapQuerySubmit(){
  dojo.connect(dijit.byId("mapQuerySubmit"), "onClick", function(){
     var formQuery = dojo.formToQuery("mapDialog");
     // Attach it into the dom as pretty-printed text.
     dojo.byId("mapDialog").innerHTML = formQuery;
  });
}

function setupSlickGrid(){
  
  var data = [];
  grid = new Slick.Grid("#varGrid", data, columns, options);
}

function SortByTerm(x,y) {
      return ((x.term == y.term) ? 0 : ((x.term > y.term) ? 1 : -1 ));
}




///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
//APIs
//Author: Sean Cleveland
//Date: 11/26/2012


function getCharacteristicNames(){
  $.get('http://localhost:8888/interactive-water-quality-mapper/CharacteristicName.xml', function(data) {
    for(i in data["Codes"]){
      characteristicNames.push(data["Codes"][i]["Code"].value)
    }
  });
};

function getAllSitesCharacteristics(){
    $.get('http://localhost:8888/iwqm-cake/variables/unique_characteristics.json', function(data) {
    for(i in data["Codes"]){
      characteristicNames.push(data["Codes"][i]["Code"].value)
    }
  });
};
